# SO, users and QLC+
- Instalar l'última Kubuntu LTS o Lubuntu LTS
- Crear un usuari llums sense sudo `sudo adduser llums`
- Habilitar autologin a l'usuari llums
  - Kubuntu (ssdm) `sudo vim /etc/sddm.conf`
    ```
    [Autologin]
    Session=Plasma
    User=llums
    ```
  - Lubuntu (ssdm) `sudo vim /etc/sddm.conf`
    ```
    [Autologin]
    Session=Lubuntu
    User=llums
    ```

- Instalar qlcplus `$sudo apt install qlcplus`
- Configurar la xarxa com a dhcp automatic, i afegir l'adreça MAC de la interficie de la LAN a la configuiració del router DHCP per vincularla a una ip fixa fora del rang de dhcp.

# Personalització de QLC i arrencada del usuari
```
cd /tmp
wget https://gitlab.com/communia/ateneu-candela/candela-llums/-/archive/main/candela-llums-main.tar.gz
tar xvzf candela-llums-main.tar.gz
cd /tmp/candela-llums-main
sudo cp start-qlc.sh /usr/local/bin/
sudo chmod +x /usr/local/bin/start-qlc.sh
sudo cp acandela_default.qxw /etc/skel/acandela_default.qxw
su llums
mkdir -p ~/Desktop
mkdir -p ~/.qlcplus
cp qlcplus-candela.desktop ~/Desktop/qlcplus-candela.desktop
cp qlcplus-candela.desktop ~/Desktop/qlcplus-candela-kiosk.desktop
mkdir -p ~/.config/autostart-scripts
ln -s /usr/local/bin/start-qlc.sh ~/.config/autostart-scripts/start-qlc.sh
exit
sudo reboot
```
